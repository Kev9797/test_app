import React from 'react'

const Table = (props) =>{
    return(
        <tbody>
            <tr>
            <th scope="row">{props.id}</th>
            <td>{props.prediccion}</td>
            </tr>
        </tbody>
    );

}

export default Table;