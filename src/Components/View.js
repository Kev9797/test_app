import React, { Fragment } from 'react';
import { useState } from 'react';
import TextField from "@material-ui/core/TextField";
import '../Styles/view.css'
import Tabla from './Table'

const Regresion = (props) =>{

    const [respuesta, setrespuesta] = useState({});
    const [State, setState] = useState({
        modelo: {
            value: '',
            required: true,
            error: false,
            label: 'Ingrese el Modelo'
          },
        profundidad: {
            value: '',
            required: true,
            error: false,
            label: 'Ingrese la Profundidad'
          },
      });

      const  handleChange = (e) => {
        let obj = null;
        obj = State.nombre;
        const { name, value } = e.target;
        switch (name) {
            
            case 'Modelo':
              obj = State.modelo;
              obj.value = value;
              obj = checkRegExp(obj,/^([0-9])*$/,'Modelo no valio','Modelo',10);
              setState( {...State, modelo: obj});
            break;

            case 'Profundidad':
              obj = State.profundidad;
              obj.value = value;
              obj = checkRegExp(obj,/^[0-9]{1,3}$|^[0-9]{1,3}\.[0-9]{1,3}$/,'Profundidad no valida','Profundidad',10);
              setState({...State, profundidad: obj });
            break;

        }
    }

    const checkRegExp = (obj, regExp, label, labelDefault, size) => {
        if (obj.value == null) {
            obj.value = '';
        }
        var longitud;
        var expresion;
  
        obj.error = (obj.value.length > size);
        longitud = obj.error;
        obj.error = !(regExp.test(obj.value));
        expresion = obj.error;
        if (longitud) {
            obj.label = `La longitud maxima es de ${size} caracteres`;
        } else if (expresion) {
            obj.label = label;
        } else {
            obj.label = labelDefault;
        }
  
        return obj;
     }
     
    const peticion = () =>{

      fetch('http://localhost:8000/modelo/?numbers='+State.modelo.value+','+State.profundidad.value)
        .then(response => {
            return response.json();
        })
        .then(response => {
            setrespuesta(response)
        })
        
        console.log("la respuesta es: ")
        console.log(respuesta);
        
    }
    
    const peticion_post = () =>{
        fetch('http://127.0.0.1:8000/say/kevin/22/')
        .then(response => {
            return response.json();
        })
        .then(response =>{
            console.log(response);
            console.log(response.message);
        })
    }

return(
    <div className="fondo-pant">
        <div className="logo">SETENAL</div>
        <div className="navbar-pop"> <img className="imagen" src="res2.jpg"></img></div>
     <div className="font-containerf">
    <div className="container-back">
        <div className="row">
            <div className="col-md-6"> 
                <TextField
                        id="outlined-name"
                        label={State.modelo.label}
                        margin="normal"
                        name="Modelo"
                        value={State.modelo.value}
                        required={State.modelo.required}
                        onChange={handleChange}
                />
            </div>
            <div className="col-md-6">
                <TextField
                        id="outlined-name"
                        label={State.profundidad.label}
                        margin="normal"
                        name="Profundidad"
                        value={State.profundidad.value}
                        required={State.profundidad.required}
                        onChange={handleChange}
                />
            </div>
            
        </div>

    </div>
     <button type="button" onClick={peticion} className="btn-aceptar"> Aceptar </button>
    </div>
    <div className="setenal">
        <table class="table table-hover table-dark">
            <thead>
                <tr>
                <th scope="col">Data</th>
                <th scope="col">Values</th>
                </tr>
            </thead>
            {Object.keys(respuesta).map((key, index) => 
                            <Tabla 
                               prediccion={respuesta[key]}
                               key={index}
                               id={key}
                            />
                        )}
            
        </table>
    </div>
</div>
)
}

export default Regresion;